﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task_2
{
    public partial class Form1 : Form
    {
        horse h = new horse();
        mybutton[,] btns = new mybutton[8, 8];
        public Form1()
        {
            InitializeComponent();
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    btns[i, j] = new mybutton(i, j);
                    btns[i, j].Location = new Point(j * 40, i * 40);
                    btns[i, j].Size = new Size(40, 40);
                    btns[i, j].x = i;
                    btns[i, j].y = j;
                    btns[i, j].BackColor = Color.White;
                    btns[i, j].Click += new System.EventHandler(btn_click);
                    this.Controls.Add(btns[i, j]);
                }
            }
        }
        private void btn_click(object sender, EventArgs e)
        {
            mybutton b = sender as mybutton;
         //   h.check(b.x, b.y);

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (i == (b.x + 2))
                    {
                        if (j == (b.y - 1) || j == (b.y + 1))
                        {
                            if (btns[i, j].BackColor == Color.White)
                            {
                                btns[i, j].BackColor = Color.Black;
                            }
                            else
                                btns[i, j].BackColor = Color.White;
                        }
                    }

                    if (i == (b.x - 2))
                    {
                        if (j == (b.y - 1) || j == (b.y + 1))
                            if (btns[i, j].BackColor == Color.White)
                            {
                                btns[i, j].BackColor = Color.Black;
                            }
                            else
                                btns[i, j].BackColor = Color.White;
                    }
                    if (i == (b.x - 1))
                    {
                        if (j == (b.y - 2) || j == (b.y + 2))
                        {
                            if (btns[i, j].BackColor == Color.White)
                            {
                                btns[i, j].BackColor = Color.Black;
                            }
                            else
                                btns[i, j].BackColor = Color.White;
                        }

                    }
                    if (i == (b.x + 1))
                    {
                        if (j == (b.y - 2) || j == (b.y + 2))
                        {
                            if (btns[i, j].BackColor == Color.White)
                            {
                                btns[i, j].BackColor = Color.Black;
                            }
                            else
                                btns[i, j].BackColor = Color.White;
                        }

                    }
                }
            }
        }

        
    }

}