﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tictactoe11
{
    public partial class Form1 : Form
    {
       public tictac t = new tictac();
       public  Mybutton[,] b = new Mybutton[3,3];
        public Form1()
        {
            InitializeComponent();
            
            for (int i = 0; i < 3; i++)
            {
               
                for (int j = 0; j < 3; j++)
                {
                  
                    b[i,j].Size = new Size(40, 40);
                    b[i,j].Location = new Point(j * 50, i * 50);
                    b[i,j].Click += new System.EventHandler(btn_click);
                    b[i,j].x = i;
                    b[i,j].y = j;
                    this.Controls.Add(b[i,j]);
                }
            }

        }
        private void btn_click(object sender, EventArgs e)
        {
            Mybutton c = sender as Mybutton;
            int ans = t.move(c.x, c.y);
            if (ans == 1)
            {
                c.Text = "X";
            }
            else if (ans == 2)
            {
                c.Text = "O";
            }
           
            if (t.win() > 0)
            {
                if (t.win() == 1)
                {
                    MessageBox.Show("first");
                }
                if (t.win() == 2)
                {
                    MessageBox.Show("second");
                }
                if (t.win() == 3)
                {
                    MessageBox.Show("nich'ya");
                }
                t.restart();
                for (int i = 0; i < 3; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        b[i,j].Text = "";
                    }
                }
            }

        }

        
        private void Form1_Load(object sender, EventArgs e)
        {
             
        }
    }
}
