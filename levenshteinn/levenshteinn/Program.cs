﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace levenshtein
{
    class Program
    {
        public static int lev_th(string w, string gw)
        {
            int[,] d = new int[w.Length + 1, gw.Length + 1];
            for (int i = 0; i < w.Length; i++)
            {
                d[i, 0] = i;
            }
            for (int j = 0; j < gw.Length; j++)
            {
                d[0, j] = j;
            }
            for (int i = 1; i < w.Length; i++)
            {

                for (int j = 1; j < gw.Length; j++)
                {
                    if (i != j)
                    {
                        d[i - 1, j - 1] = d[i - 1, j - 1] + 2;
                    }
                    else
                    {
                        d[i - 1, j - 1] = d[i - 1, j - 1];
                    }
                    d[i, j] = Math.Min(Math.Min(d[i, j - 1] + 1, d[i - 1, j] + 1), d[i - 1, j - 1]);

                }
            }
            return d[w.Length, gw.Length];

        }


        static void Main(string[] args)
        {

            StreamReader sr = new StreamReader(@"C:\Новая папка\dict.txt");
            //StreamReader sw = new StreamReader(@"C:\Новая папка\text.txt.txt");
            //string[] words = new string[60000];
            // string s = sw.ReadToEnd();
            int sz = 0;
            string[] words = new string[10000];
            while (sr.Peek() != -1)
            {
                words[sz] = sr.ReadLine();
                sz++;
            }
            string s = "book shelf";
            string[] giv_words = s.Split(' ');
            for (int i = 0; i < giv_words.Length; i++)
            {
                int min_dist = -1;
                for (int j = 0; j < sz; j++)
                {
                    if (lev_th(giv_words[i], words[j]) < min_dist || min_dist == -1)
                    {
                        min_dist = lev_th(giv_words[i], words[j]);
                    }
                }
                if (min_dist == 0)
                {
                    Console.WriteLine("good");
                }

                else
                {
                    for (int j = 0; j < sz; j++)
                    {
                        if (lev_th(giv_words[i], words[j]) == min_dist)
                        {
                            Console.WriteLine(words[j]);
                        }
                    }
                }
                Console.ReadKey();
            }
        }
    }

}

