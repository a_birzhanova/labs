﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Show_Files_Demo
{
    
    class Program
    {
        static void ShowDirectory (DirectoryInfo dir)
        {
            foreach (FileInfo file in dir.GetFiles())
                {
                     Console.WriteLine("File:{0}", file.FullName);
            }
            foreach (DirectoryInfo subdir in dir.GetDirectories())
            {   
                ShowDirectory(subdir);
            }
        }

        static void Main(string[] args)
        {
            DirectoryInfo dir = new DirectoryInfo(Environment.SystemDirectory);
            ShowDirectory(dir);
        }
    }
}
