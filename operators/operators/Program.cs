﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace operators
{
    struct DNumber
    {
        int a, b;
        public DNumber(int _a, int _b)
        {
            a = _a;
            b = _b;
        }
        public override string ToString()
        {
            return a.ToString() + "/" + b.ToString();

        }
        public void Normalize(){
            int x = Math.Abs(a);
            int y = Math.Abs(b);
            while (x > 0 && y > 0)
            {
                if (x > y)
                {
                    x = x % y;
                }
                else
                    y = y % x;
            }
            x = x + y;
            a /= x;
            b /= x;
        }
        public static DNumber operator *(DNumber arg1, DNumber arg2){
            arg1.a *=arg2.a;
            arg1.b *=arg2.b;
            arg1.Normalize();
            return arg1;
        }
        public static DNumber operator +(DNumber arg1, DNumber arg2)
        {
            
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.b *= arg2.b;
            arg1.a += arg2.a;
            arg1.Normalize();
            return arg1;
        }
        public static DNumber operator -(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg2.a *= arg1.b;
            arg1.b *= arg2.b;
            arg1.a -= arg2.a;
            arg1.Normalize();
            return arg1;

        }
        public static DNumber operator /(DNumber arg1, DNumber arg2)
        {
            arg1.a *= arg2.b;
            arg1.b *= arg2.a;
            arg1.Normalize();
            return arg1;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            string s1 = Console.ReadLine();
            string[] sa = s.Split();
            DNumber x = new DNumber(int.Parse(sa[0]), int.Parse(sa[1]));

            sa = s1.Split();
            DNumber y = new DNumber(int.Parse(sa[0]), int.Parse(sa[1]));

            DNumber z = x * y;
            DNumber p = x + y;
            DNumber m = x - y;
            DNumber d = x / y;
            Console.WriteLine(z);
            Console.WriteLine(p);
            Console.WriteLine(m);
            Console.WriteLine(d);
            Console.ReadKey();


        }
    }
}
