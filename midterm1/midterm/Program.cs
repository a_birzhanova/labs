﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace midterm
{
    class Point
    {
        int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;

        }
        public static Point operator + (Point arg1,Point arg2)
        {
            Point arg = new Point (arg1.x+arg2.x,arg1.y+arg2.y);
            return arg;
        }
        public override string ToString()
        {
            return x.ToString()+ " " + y.ToString();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Point a = new Point(3,5);
            Point b = new Point(4, 5);
            Point c = a + b;
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
