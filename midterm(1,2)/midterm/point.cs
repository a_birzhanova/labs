﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace midterm
{
    class Point
    {
        
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;

        }
        public static Point operator +(Point arg1, Point arg2)
        {
            Point arg = new Point(arg1.x + arg2.x, arg1.y + arg2.y);
            return arg;
        }
        public override string ToString()
        {
            return x.ToString() + " " + y.ToString();
        }

      
    }
    }

